import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public base64Image: string;
  public errorMessage : string;
  constructor(public navCtrl: NavController, public camera: Camera) {
this.errorMessage = "starting";

  }

  capture() {
    
    
    const cameraOptions: CameraOptions = {

        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    

    }


    this.camera.getPicture(cameraOptions).then((imageData) => {

      this.errorMessage = 'photo taken';

      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image =  'data:image/jpeg;base64,' + imageData;
      
    }, (err) => {
      // Handle error
      alert(err);
      this.errorMessage = 'oops, something went wrong' + err;
    });


  }

}
